import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

import * as microsoftTeams from "@microsoft/teams-js";


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    microsoftTeams.initialize(() => {
      this.authService.logoutCallback()
      .then(resp => {
        microsoftTeams.authentication.notifySuccess();
      })
      .catch(error => {
        microsoftTeams.authentication.notifyFailure('Error');
      });
    })
  }
}
