import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import * as microsoftTeams from "@microsoft/teams-js";

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  isLoggedIn = new Subject<boolean>();

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.updateLoginStatus();
  }

  onLogin(): void {
    this.authService.getLoginUrl()
      .then(url => {
        const encodedUrl = encodeURIComponent(url);

        microsoftTeams.initialize(() => {
          microsoftTeams.authentication.authenticate({
            url: 'redirect?redirectUrl=' + encodedUrl,
            width: 600,
            height: 535,
            successCallback: (result) => {
              if(result) {
                this.authService.addUser(result);
                this.updateLoginStatus();
              } else {
                window.alert('Fail! Callback should provide User.');
              }
            },
            failureCallback: function (reason) {
              window.alert('fail: ' + reason);
            }
          });
        })
      });
  }

  onLogOut(): void {
    this.authService.getLogOutUrl().then(url => {
      const encodedUrl = encodeURIComponent(url);

      microsoftTeams.initialize(() => {
        microsoftTeams.authentication.authenticate({
          url: 'redirect?redirectUrl=' + encodedUrl,
          width: 600,
          height: 535,
          successCallback: (result) => {
            this.updateLoginStatus();
          },
          failureCallback: (reason) => {
            window.alert('failure');
            this.updateLoginStatus();
          }
        });
      });
    });
  }

  private updateLoginStatus(): void {
    this.authService.getUser()
      .then(user => {
        const isLoggedIn = user !== null
        this.isLoggedIn.next(isLoggedIn);
      });
  }
}