import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-redirect-test',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {

  redirectUrl: String = '';
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    if(redirectUrl) {
      this.redirectUrl = redirectUrl;
      window.location.href = redirectUrl;
    } else {
      console.log('No redirect uri was provided!');
    }
  }
}
