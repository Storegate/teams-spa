import { Component, OnInit } from '@angular/core';
import * as microsoftTeams from "@microsoft/teams-js";

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    microsoftTeams.initialize(() => {
      this.authService.loginCallback()
        .then(user => {
          microsoftTeams.authentication.notifySuccess(user.toStorageString());
        })
        .catch(error => {
          microsoftTeams.authentication.notifyFailure('Error');
        });
    });
  }
}
