import { Injectable } from '@angular/core';
import { UserManager, User, SignoutResponse, UserManagerSettings, WebStorageStateStore } from 'oidc-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userManager: UserManager;

  constructor() {
    const settings: UserManagerSettings = {
      authority: environment.stsAuthority,
      client_id: environment.clientId,
      redirect_uri: environment.clientRoot + '/signin-callback',
      automaticSilentRenew: false,
      monitorSession: false,
      post_logout_redirect_uri: environment.clientRoot,
      response_type: 'id_token token',
      scope: environment.clientScope,
      client_secret: 'C1308F3E-48F2-4E76-8556-4D9CC34B6BA1',
      userStore: new WebStorageStateStore({ store: window.localStorage })
    };
    this.userManager = new UserManager(settings);
  }

  public getLoginUrl(): Promise<string> {
    return this.userManager.createSigninRequest()
      .then(signinRequest => {
        return signinRequest.url
      });
  }

  public getLogOutUrl(): Promise<string> {
    return this.getUser()
      .then(user => {

        const settings = {
          post_logout_redirect_uri: environment.clientRoot + '/signout-callback',
          id_token_hint: user?.id_token
        };
    
        return this.userManager.removeUser()
          .then(() => {
            return this.userManager.createSignoutRequest(settings)
              .then(signoutRequest => {
                return signoutRequest.url
              });
          });
      });
  }

  public logoutCallback(): Promise<SignoutResponse | undefined> {
    return this.userManager.signoutCallback()
  }

  public loginCallback(): Promise<User> {
    return this.userManager.signinRedirectCallback();
  }

  public getUser(): Promise<User | null> {
    return this.userManager.getUser();
  }

  public addUser(userString: string): Promise<void> {
    const user = User.fromStorageString(userString);
    return this.userManager.storeUser(user);
  }
}
