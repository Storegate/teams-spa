import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartComponent } from '../components/start/start.component';
import { LoginComponent } from '../components/login/login.component';
import { LogoutComponent } from '../components/logout/logout.component';
import { RedirectComponent } from '../components/redirect/redirect.component';
import { ConfigComponent } from '../config/config.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: 'start', component: StartComponent },
  { path: 'signout-callback', component: LogoutComponent },
  { path: 'signin-callback', component: LoginComponent},
  { path: 'redirect', component: RedirectComponent},
  { path: 'config', component: ConfigComponent}
];

@NgModule({
  imports: [
      RouterModule.forRoot(appRoutes),
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule { }